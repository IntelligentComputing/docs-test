# Chapter 1
$$
  \sqrt{x}\\
  \sqrt[3]{x}
$$

$$
D(x) = \begin{cases}
\lim \limits_{x \to 0} \frac{a^x}{b+c},&x<3 \\
\pi,&x=3\\
\int_a^{3b}x_{ij}+e^2dx,&x>3\\
\end{cases}
$$

## 图片

![](./images/logo.png)

## 网页方式加载图片
<image src='./images/logo.png' style='zoom:10%'>




